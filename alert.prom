  - alert: smart_devices_without_short_self_tests
    expr: count(smartmon_device_power_on) - (count(smartmon_device_power_on - on(node,device) group_right max without (entry) (smartmon_device_self_tests{test="short"})) or vector(0)) > 0
    for: 5m
    labels:
      severity: critical
    annotations:
      summary: "A total of {{$value}} S.M.A.R.T. devices are running without any logged short self tests"
      description: "A total of {{$value}} S.M.A.R.T. devices are running without any logged short self tests."

  - alert: smart_devices_without_long_self_tests
    expr: count(smartmon_device_power_on) - (count(smartmon_device_power_on - on(node,device) group_right max without (entry) (smartmon_device_self_tests{test="long"})) or vector(0)) > 0
    for: 5m
    labels:
      severity: critical
    annotations:
      summary: "A total of {{$value}} S.M.A.R.T. devices are running without any logged long self tests"
      description: "A total of {{$value}} S.M.A.R.T. devices are running without any logged long self tests."


  - alert: smart_device_overdue_short_self_tests
    expr: floor((smartmon_device_power_on - on(instance,device) group_right max without (entry) (smartmon_device_self_tests{test="short"})) / 24) > 14
    for: 15s
    labels:
      severity: critical
    annotations:
      summary: "last short self test on device {{ $labels.device }} (host {{ $labels.instance }}) was executed {{ $value }} days ago"
      description: "last short self test on device {{ $labels.device }} (host {{ $labels.instance }}) was executed {{ $value }} days ago"


  - alert: smart_device_overdue_long_self_tests
    expr: floor((smartmon_device_power_on - on(instance,device) group_right max without (entry) (smartmon_device_self_tests{test="long"})) / 24) > 45
    for: 15s
    labels:
      severity: critical
    annotations:
      summary: "last long self test on device {{ $labels.device }} (host {{ $labels.instance }}) was executed {{ $value }} days ago"
      description: "last long self test on device {{ $labels.device }} (host {{ $labels.instance }}) was executed {{ $value }} days ago"


  - alert: smart_device_overdue_long_self_tests
    expr: floor((smartmon_device_power_on - on(instance,device) group_right max without (entry) (smartmon_device_self_tests{test="long"})) / 24) > 45
    for: 5m
    labels:
      severity: critical
    annotations:
      summary: "last long self test on device {{ $labels.device }} (host {{ $labels.instance }}) was executed {{ $value }} days ago"
      description: "last long self test on device {{ $labels.device }} (host {{ $labels.instance }}) was executed {{ $value }} days ago"

  - alert: smart_device_offline_data_collection_disabled
    expr: smartmon_device_data_collection{status="128"}
    for: 5m
    labels:
      severity: warning
    annotations:
      summary: "auto offline data collection for device {{ $labels.device }} (host {{ $labels.instance }}) is disabled"
      description: "auto offline data collection for device {{ $labels.device }} (host {{ $labels.instance }}) is disabled."

  - alert: smart_device_smart_disabled
    expr: smartmon_device_smart_enabled == 0
    for: 5m
    labels:
      severity: critical
    annotations:
      summary: "S.M.A.R.T disabled on device {{ $labels.device }} (host {{ $labels.instance }})"
      description: "S.M.A.R.T disabled on device {{ $labels.device }} (host {{ $labels.instance }})"

  - alert: smart_device_errors_increased
    expr: increase(smartmon_device_errors[24h]) > 0
    for: 5m
    labels:
      severity: warning
    annotations:
      summary: "S.M.A.R.T errors increased over the last 24 hours on device {{ $labels.device }} (host {{ $labels.instance }})"
      description: "S.M.A.R.T errors increased over the last 24 hours on device {{ $labels.device }} (host {{ $labels.instance }})"

  # "airflow_temperature_cel" only for ssds?
  - alert: smart_device_running_hot
    expr: smartmon_attr_raw_value{name="airflow_temperature_cel"} >= 45
    for: 5m
    labels:
      severity: warning
    annotations:
      summary: "S.M.A.R.T reports a temperature over 45°C on device {{ $labels.device }} (host {{ $labels.instance }}), this can slightly effect the device health"
      description: "S.M.A.R.T reports a temperature over 45°C on device {{ $labels.device }} (host {{ $labels.instance }}), this can slightly effect the device health."

  # "smart_device_estimated_wear_leveling" only for ssds
  - alert: smart_device_estimated_wear_leveling
    expr: smartmon_attr_value{name="wear_leveling_count"} < 30
    for: 5m
    labels:
      severity: warning
    annotations:
      summary: "S.M.A.R.T reports an estimated remaining life of {{ $value }}% according to the device {{ $labels.device }} (host {{ $labels.instance }}) wear leveling count"
      description: "S.M.A.R.T reports an estimated remaining life of {{ $value }}% according to the device {{ $labels.device }} (host {{ $labels.instance }}) wear leveling count."
